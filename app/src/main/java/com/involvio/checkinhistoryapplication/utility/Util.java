package com.involvio.checkinhistoryapplication.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Utility class
 * Created by Niharika on 13-05-2018.
 */

public class Util {


    private static Util util;

    private Util() {
    }

    private static void createInstance() {
        if (util == null)
            util = new Util();
    }

    public static Util getInstance() {
        if (util == null)
            createInstance();
        return util;
    }
    /**
     * Get time with am/pm marker
     *
     * @param date -Date and time on which event done
     * @return formatted time string
     */
    public String getFormattedTime(String date) {
        try {

            Date parse = this.getSimpleDateFormat().parse(date);
            return new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(parse);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String formatCheckoutTime(Date date) {
        return this.getSimpleDateFormat().format(date);
    }

    SimpleDateFormat getSimpleDateFormat() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        return simpleDateFormat;
    }
}
