package com.involvio.checkinhistoryapplication.utility;


import com.involvio.checkinhistoryapplication.model.Order;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class CheckoutHistory {

    private Calendar calendar;

    private ArrayList<Order> orders = new ArrayList<>();

    public CheckoutHistory(String date, Order order) {

        calendar = Calendar.getInstance();
        try {
            calendar.setTime(Util.getInstance().getSimpleDateFormat().parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        orders.add(order);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CheckoutHistory that = (CheckoutHistory) o;
        return calendar.get(Calendar.YEAR) == that.calendar.get(Calendar.YEAR) &&
                calendar.get(Calendar.MONTH) == that.calendar.get(Calendar.MONTH) &&
                calendar.get(Calendar.DAY_OF_MONTH) == that.calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public int hashCode() {
        return calendar.get(Calendar.YEAR) + calendar.get(Calendar.MONTH) + calendar.get(Calendar.DAY_OF_MONTH
        );
    }

    private String getMonthName() {
        return calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
    }

    private String getDayName() {
        return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
    }

    private String getDate() {
        return calendar.get(Calendar.DAY_OF_MONTH) + getDaySuffix(calendar.get(Calendar.DAY_OF_MONTH));
    }

    private String getDaySuffix(final int n) {
        if (n < 1 || n > 31)
            return "Invalid date";
        if (n >= 11 && n <= 13)
            return "th";

        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    /**
     * @return Date and Week day in proper format
     */
    // Method to set date and time in format
    public String getDateAndTime() {
        return this.getMonthName() + " " +
                this.getDate() + " - " + this.getDayName();
    }

}
